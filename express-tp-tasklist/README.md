# Express TP: Liste des tâches

Le but de ce tp est de créer une application pour réaliser la gestion de liste de tâche.
Cette application devra répondre au critére suivant :

- Je peux crée / consulter / supprimer une liste de tâche
- Je peux crée / consulter / éditer une tâche
- Je peux supprimer une tâche si elle n'est associé à aucune liste de tâche
- Depuis un guide je peux visualiser les tâches et avoir un lien pour consulter leurs détails.

Vous devrez crée de zéro les fichiers `TaskListRouter` et `TaskRouter`.

## Installation des dépendances

Les paquets utilisé pour ce tp doivent être installer via la commande suivante.

```bash
npm install
```

## Executer le tp

Il est nécessaire d'utiliser la commande ci-dessous car l'architecture des sources de l'application n'est pas la même.
Node cherche toujours le dossier node_modules la ou la commande `node` est executer. 

```bash
npm start
```

## Etape 1: Création des routers

Dans cette premiére partie, le but est de créer les différents routers de notre application.
Ils devront répondre au routes suivantes, attention les routes contenant des id devront être variabiliser pour convenir peut importe la valeur :

TaskRouter

- `/task/`
    GET doit afficher toutes les tâches
- `/task/display/0` 
    GET doit afficher la tache 0
- `/task/edit/0`
    GET affiche un formulaire pour éditer la tache 0.
    POST applique les changements sur la tâche 0
- `/task/delete/0`
    POST supprime la tâche 0
- `/task/new`
    GET affiche un formulaire pour l'ajout d'une tâche
    POST crée la nouvelle tâche

TaskListRouter

- `/list/`
    GET doit afficher toutes les listes de tâches
- `/list/0`
    GET doit afficher les tâches de liste 0
- `/list/0/edit`
    GET affiche un formulaire pour éditer la liste 0.
    POST applique les changements sur la liste 0
- `/list/0/delete`
    POST supprime la liste 0
- `/list/new`
    GET affiche un formulaire pour l'ajout d'une liste
    POST crée une nouvelle liste

## Etape 2: Filtrer les requêtes

Dans les cas comme `/task/edit/0` on souhaiterait avant d'éxecuter la méthode principale récupérer la tâche avant.

Vous devrez crée un middleware à éxecuter avant le c'est route. Si il parvient à trouver la tâche alors il doit l'ajouter à la requête puis continuer l'éxecution.
Si elle est manquante alors il doit intérrompre l'éxecution et retourner erreur avec le code 404.

Vous devrez alors dans votre handler principal avoir `request.task` qui doit être défini et non null.

Faire de même avec les listes de tâches.

## Etape 3: Afficher les données

Express peut être étendu avec de nombreux moteur de template. Ici nous utiliseront pug. Vous devrez créer les différentes pages correspondantes et les appellé aux bonnes routes. Le but ici n'est pas faire de copier coller mais d'utiliser les fonctionnalité de pug pour facilité l'écriture de celle-ci.

## Etape 4: Récupération d'un formulaire

Comme à l'étape 2 crée un middelware permettant de récupérer le formulaire et de le passer à la requête suivante.

Pour `/task/edit/0` en post vous devrez pouvoir éxecuter vos 2 middelware. Il suiffera dans le handler principal d'appliquer les modifications.

## Pour aller plus loin

La plus part des entreprises vous demanderont de tester votre application.
C'est la que `mocha`, `chai` et `chai-http`.
Vous avez un exemple de test crée dans le dossier test.
Le but ici serait de tester le comportement de notre application.

```bash
npm test
```