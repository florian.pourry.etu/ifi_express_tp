const express = require( "express" );
const app = express();

module.exports = function( hdl ){

    const bodyParser = require('body-parser')
    app.use( bodyParser.json() );       // to support JSON-encoded bodies
    app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
    })); 

    app.set('view engine', 'pug');
    app.set('views', `${ __dirname }/web` );

    const TaskListRouter = require( "./TaskListRouter" );
    app.use( "/list", TaskListRouter );

    const TaskRouter = require( "./TaskRouter" );
    app.use( "/task", TaskRouter );

    hdl( app );
};