const chai = require( "chai" );
const chai_http = require( "chai-http" );
const init = require( "../src/init" );
chai.use( chai_http );

init( app => {

app.listen( 8080, () => {

describe( "Express TP TaskList", () => {

    it( "request has answer", (done) => {

        chai.request( "http://127.0.0.1" )
        .get( "/" )
        .end(( err, res ) => {
            res.should.have.status( 200 );
            done();
        });

    });

});

});

});