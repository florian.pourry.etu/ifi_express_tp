class Registry {

    /**
     * Build a new registry
     */
    constructor(){
        this.container = [];
    }

    /**
     * Find an item of the registry
     * 
     * @param {Number | Function} seeker is the criterium to find an item of the registry
     * 
     * @returns an item from the registry or null if missing
     */
    find( seeker ){

        let seekerUnited;
        if( seeker instanceof Function ){
            seekerUnited = seeker;
        } else {
            seekerUnited = (id) => id === seeker;
        }

        for ( let index = 0; index < this.container.length; index++ )
        if ( seekerUnited( index, this.container[ index ] ) ){
            return {
                id: index,
                item: this.container[ index ]
            };
        }

        return null;
    }

    /**
     * Get the item id or null if the item missing
     * 
     * @param {Type} item is the item seeked
     * 
     * @returns the id of the item
     */
    findId( item ){

        for ( let index = 0; index < this.container.length; index++ )
        if ( item === this.container[ index ] ){ 
            return index;
        } 

        return null;
    }

    /**
     * Add a new item to the registry
     * 
     * @param {Type} item is the new task to be added
     * 
     * @returns the id of the item
     */
    insert( item ){
        return this.findId( item ) || (
            this.container.push( item ) - 1
        );
    }

    delete( id ){
        let [item] = this.container.splice( id, 1 );
        return item;
    }

    all(){
        let list = [];

        for ( let index = 0; index < this.container.length; index++ ){
            list.push({
                id: index,
                item: this.container[ index ]
            });
        }

        return list;
    }
}

const TaskRegistry = new Registry();
exports.TaskRegistry = TaskRegistry;

const TaskListRegistry = new Registry();
exports.TaskListRegistry = TaskListRegistry;